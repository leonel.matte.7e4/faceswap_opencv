# OpenCV Project (Face swap)

- Objetivo: Intercambiar las caras de dos personas a traves de la libreria OpenCV.

A grosso modo necesitamos:
1. Reconocer si es una cara o no.
2. Aislar el contorno de la cara.
3. Reajustar rasgos.
4. Remplazar una cara por la otra.
5. Reajustar colores.

Principales referencias para desempeñar el proyecto
- https://github.com/wuhuikai/FaceSwap
- https://learnopencv.com/face-swap-using-opencv-c-python/

**Overview del código**

_source_ = será la imagen del rostro que deseamos pegar.

_destination_ = será la imagen del rostro en el cual deaseamos pegar la cara previamente mencionada. 

**Importación y preparación de las imagenes**
 
`img = cv2.imread("images/vin_diesel.jpg")`

`img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)`

`mask = np.zeros_like(img_gray)`

`img2 = cv2.imread("images/jason_statham.jpg")`

`img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)`

**Encontrar los "landmark points" de las caras**

dlib libreria requerida*

- Dlib nos permite obviar la primera fase (reconocer si es una cara o no) a traves del metodo asociado a la variable detector.

- A traves de la variable predictor se encuentran los "landmark points" hacemos un primer trazo del contorno de las caras pasandole como parametro un modelo ya entrenado.  

`detector = dlib.get_frontal_face_detector()`

`predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")`

`faces = detector(img_gray)`

`for face in faces:`

`landmarks = predictor(img_gray, face)`

`landmarks_points = []`

`for n in range(0, 68):`

`x = landmarks.part(n).x`

`y = landmarks.part(n).y`

`landmarks_points.append((x, y))`

**Acabar de trazar el contorno de las caras**

- Para esto haremos uso de "Delaunay Triangulation" que basicamente segmenta la cara en triangulos para luego reacomodar mejor la imagen _source_ a la _destination_ . Hacemos esto debido a que queremos tanto mantener las proporciones originales de las rasgos faciales y poder redefinir el tamaño junto a la perspectiva de los rasgos del rostro.
 
`Delaunay triangulation`

`rect = cv2.boundingRect(convexhull)`

`subdiv = cv2.Subdiv2D(rect)`

`subdiv.insert(landmarks_points)`

`triangles = subdiv.getTriangleList()`

`triangles = np.array(triangles, dtype=np.int32)`

**Delaunay Triangulation con la imagen _destination_**

- Se necesita usar el mismo patrón/segmento de triangulos de la cara _source_ en la cara _destination_ para poder hacer el intercambio de las dos caras. 

- Recuperaremos de la variable _triangles_ los landmark points de la source_image para poder conseguir el mismo patrón de triangulos en la cara _destination_ .


`indexes_triangles = []`

`for t in triangles:`

`pt1 = (t[0], t[1])`

`pt2 = (t[2], t[3])`

`pt3 = (t[4], t[5])`

`index_pt1 = np.where((points == pt1).all(axis=1))`

`index_pt1 = extract_index_nparray(index_pt1)`

`index_pt2 = np.where((points == pt2).all(axis=1))`

`index_pt2 = extract_index_nparray(index_pt2)`

`index_pt3 = np.where((points == pt3).all(axis=1))`

`index_pt3 = extract_index_nparray(index_pt3)`

`if index_pt1 is not None and index_pt2 is not None and index_pt3 is not None:`

`triangle = [index_pt1, index_pt2, index_pt3]`

`indexes_triangles.append(triangle) `


**Redefinicion de la cara _source_**

- Seguidamente para poder pegar el rostro y que calze mejor, moldearemos los triangulos para que correspondan tanto en tamaño y en perspectiva a los respectivos rasgos de la cara _destination_.


`points = np.float32(points)`

`points2 = np.float32(points2)`

`M = cv2.getAffineTransform(points, points2)`

`warped_triangle = cv2.warpAffine(cropped_triangle, M, (w, h))`

`warped_triangle = cv2.bitwise_and(warped_triangle, warped_triangle, mask=cropped_tr2_mask)`

**Reconstrucción del segmento de triangulos redefinidos**

- Usamos el patrón de triangulos modificados anteriormente para reconstruir la cara. 

**Hora de remplazar las caras**

- Cortamos lo que correspondería a la superficie de nuestro segmento de triangulos en la imagen _destination_ pegamos el patrón de _source_. 
 
`img2_face_mask = np.zeros_like(img2_gray)`

`img2_head_mask = cv2.fillConvexPoly(img2_face_mask, convexhull2, 255)`

`img2_face_mask = cv2.bitwise_not(img2_head_mask)`

`img2_head_noFace = cv2.bitwise_and(img2, img2, mask=img2_face_mask)`

`result = cv2.add(img2_head_noFace, img2_new_face)`

- Para acabar, ajustamos los colores para igualar las tonalidades de la imagen _destination_. 

`(x, y, w, h) = cv2.boundingRect(convexhull2)`

`center_face2 = (int((x + x + w) / 2), int((y + y + h) / 2))`

`swapped_face = cv2.seamlessClone(result, img2, img2_head_mask, center_face2, cv2.   NORMAL_CLONE)`

<img src="https://gitlab.com/leonel.matte.7e4/faceswap_opencv/-/raw/master/images/vin_diesel.jpg" width="230" height="300">

Source


<img src="https://gitlab.com/leonel.matte.7e4/faceswap_opencv/-/raw/master/images/jason_statham.jpg" width="200" height="300">

Destination


<img src="https://gitlab.com/leonel.matte.7e4/faceswap_opencv/-/raw/master/images/Swapped%20Face%20Result_screenshot_22.04.2021.png" width="200" height="300">

Result


_Et voila, c'est fini!_ 
